package com.example.yin.minesweeper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class CustomView extends View
{
    private ArrayList<makingListners> markingChange;
    private ArrayList<finishListners> finishs;
    private float dimension;
    private float cell_dimension;

    private Paint paint;
    private Paint paint_text;

    private float[] yline;
    private float[] xline;

    private Point detectedCell;
    private Cell[][] cells;

    private boolean markingMode = false;
    private boolean failed = false;
    private int markedCount;

    // ctor
    public CustomView(Context context)
    {
        super(context);
        init();
    }
    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public CustomView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setCell(Cell[][] cells)
    {
        this.cells = cells;
        invalidate();
    }

    private void init()
    {
        markingChange = new ArrayList<>();
        finishs = new ArrayList<>();
        this.markingMode = false;
        this.failed = false;
        this.markedCount = 0;

        paint = new Paint();
        paint_text = new Paint();
        paint_text.setTextSize(50);
        paint.setStrokeWidth(2.0f);
    }


    // gets called very often
    public void onDraw(Canvas canvas)
    {
        //super.onDraw(canvas);
        paint.setColor(Color.BLACK);
        canvas.drawRect(0, 0, this.dimension, this.dimension, paint);

        paint.setColor(Color.WHITE);

        canvas.drawLines(this.xline, paint);
        canvas.drawLines(this.yline, paint);

        for(int i = 0; i < this.cells.length; i++)
        {
            for(int j = 0; j < this.cells[i].length; j++)
            {
                if(!this.cells[i][j].isCovered())
                {
                    paint.setColor(Color.GRAY);
                    paint_text.setColor(Color.RED);
                    switch (this.cells[i][j].getMCount())
                    {
                        case -1:
                            if(this.cells[i][j].isMarked())
                            {

                                paint.setColor(Color.YELLOW);
                                paint_text.setColor(Color.BLACK);
                                drawCell(canvas,i,j,paint, "M", paint_text);
                            }
                            else
                            {
                                paint.setColor(Color.RED);
                                paint_text.setColor(Color.BLACK);
                                drawCell(canvas,i,j,paint, "M", paint_text);
                            }
                            break;
                        case 0:
                            drawCell(canvas,i,j,paint);
                            break;
                        case 1:
                            paint_text.setColor(Color.BLUE);
                            drawCell(canvas,i,j,paint, "1",  paint_text);
                            break;
                        case 2:
                            paint_text.setColor(Color.GREEN);
                            drawCell(canvas,i,j,paint, "2",  paint_text);
                            break;
                        case 3:
                            paint_text.setColor(Color.YELLOW);
                            drawCell(canvas,i,j,paint, "3", paint_text);
                            break;
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                            drawCell(canvas,i,j,paint, String.valueOf(this.cells[i][j].getMCount()),paint_text);
                            break;
                        default:
                            break;
                    }
                }
                else
                {

                    if(this.cells[i][j].isMarked())
                    {
                        paint.setColor(Color.YELLOW);
                        drawCell(canvas,i,j,paint);
                    }
                    else
                    {
                        paint.setColor(Color.BLACK);
                        drawCell(canvas,i,j,paint);
                    }
                }


            }
        }


    }

    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent event)
    {
        if(!this.failed)
        {
            // return super.onTouchEvent(event);
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                double x = (double) event.getX();
                double y = (double) event.getY();
                int column = (int) (x / cell_dimension);
                int row = (int) (y / cell_dimension);
                this.detectedCell = new Point(row, column);

            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                double x = (double) event.getX();
                double y = (double) event.getY();
                int row = (int) (y / cell_dimension);
                int column = (int) (x / cell_dimension);

                // same cell pressed and released
                if (this.detectedCell.equals(row, column))
                {
                    // uncover cells
                    if(!this.markingMode)
                    {
                        // not marked
                        if(!this.cells[row][column].isMarked())
                        {
                            this.cells[row][column].setCovered(false);
                            // uncovered a mine
                            if (this.cells[row][column].getMCount() == -1)
                            {
                                this.setFailed(true);
                                this.uncover_all_mines();
                            }
                            else if(this.cells[row][column].getMCount() == 0)
                            {
                                this.uncover_nearby_cells(row, column);
                            }
                        }
                    }
                    // marking mode
                    else
                    {
                        if(this.cells[row][column].isCovered())
                        {
                            if(!this.cells[row][column].isMarked())
                            {
                                if(this.checkMarkedCount())
                                {
                                    this.cells[row][column].setMarked(true);
                                    this.setMarkedCount(this.getMarkedCount() + 1);
                                }
                            }
                            else if(this.cells[row][column].isMarked())
                            {
                                this.cells[row][column].setMarked(false);
                                this.setMarkedCount(this.getMarkedCount() -1);
                            }


                        }
                    }
                    if(check_if_game_won())
                    {
                        for(finishListners listener : finishs)
                        {
                            listener.onFinished(true);
                        }
                    }

                }
            }

        }
        invalidate();
        return true;
    }

    private boolean check_if_game_won()
    {
        if(this.failed)
        {
            return false;
        }
        for (Cell[] cell : this.cells) {
            for (Cell aCell : cell) {
                if (!aCell.isMarked() && aCell.isCovered()) {
                    return false;
                }

            }
        }
        return true;
    }

    private void uncover_all_mines()
    {
        for (Cell[] cell : this.cells) {
            for (Cell aCell : cell) {
                if (aCell.getMCount() == -1) {
                    aCell.setCovered(false);
                }
            }
        }
    }


    private void uncover_nearby_cells(int row, int column)
    {
        for(int i = -1; i < 2; i++)
        {
            for(int j = -1; j < 2; j++)
            {
                if(row+i < 0 || row+i >= this.cells.length ||
                        column+j < 0 || column+j >= this.cells.length)
                {
                    continue;
                }
                if(this.cells[row+i][column+j].isCovered())
                {
                    this.cells[row+i][column+j].setCovered(false);
                    if(this.cells[row+i][column+j].isMarked())
                    {
                        this.cells[row+i][column+j].setMarked(false);
                        this.setMarkedCount(this.getMarkedCount() + -1);
                    }

                    if(this.cells[row+i][column+j].getMCount() == 0)
                    {
                        uncover_nearby_cells(row+i, column+j);
                    }
                }



            }
        }
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setPadding(0,0,0,0);

        int height = getMeasuredHeight();
        int width = getMeasuredWidth();

        if (height > width)
        {
            this.dimension = width;
        }
        else
        {
            this.dimension = height;
        }

        this.cell_dimension = this.dimension/10;

        this.yline = new float[44];
        for(int i = 0; i < this.yline.length; i=i+4)
        {
            if(i == this.yline.length-4)
            {
                // first point x, y
                this.yline[i] = this.cell_dimension * (i/4)-1;
                this.yline[i+1] = 0f;
                // second point x, y
                this.yline[i+2] = (this.cell_dimension * (i / 4)) - 1;
                this.yline[i+3] = this.dimension-1;
                continue;
            }
            // first point x, y
            this.yline[i] = this.cell_dimension * (i/4);
            this.yline[i+1] = 0f;
            // second point x, y
            this.yline[i+2] = this.cell_dimension * (i/4);
            this.yline[i+3] = this.dimension;
        }

        this.xline = new float[44];
        // each iteration is for one line
        for(int i = 0; i < this.xline.length; i=i+4)
        {
            if(i == this.xline.length-4)
            {
                // first point x, y
                this.xline[i] = 0f;
                this.xline[i+1] = this.cell_dimension * (i/4) -1;
                // second point x, y
                this.xline[i+2] = this.dimension-1;
                this.xline[i+3] = (this.cell_dimension*(i/4))-1;
                continue;
            }
            // first point x, y
            this.xline[i] = 0f;
            this.xline[i+1] = this.cell_dimension * (i/4);
            // second point x, y
            this.xline[i+2] = this.dimension;
            this.xline[i+3] = this.cell_dimension*(i/4);

        }

        setMeasuredDimension((int) this.dimension, (int) this.dimension);
    }

    private boolean checkMarkedCount()
    {
        return this.markedCount < 20;
    }

    public void drawCell(Canvas canvas, int row, int column, Paint cellBackgroundColor)
    {
        // left, top, right, bot, color
        float left = (column * this.cell_dimension) + 2.0f;
        float top = (row *this.cell_dimension) + 2.0f;
        float right = ((column+1) * this.cell_dimension) - 2.0f;
        float bottom = ((row+1) *this.cell_dimension) - 2.0f;

        canvas.drawRect(left, top, right, bottom, cellBackgroundColor);

    }
    public void drawCell(Canvas canvas, int row, int column, Paint cellBackgroundColor, String cellText, Paint cellTextColor)
    {
        drawCell(canvas, row, column, cellBackgroundColor);
        drawText(canvas, cellTextColor, cellText, row, column);
    }

    public void drawText(Canvas canvas, Paint paint, String cellText, int row, int column)
    {

        int left = (int) ((column * this.cell_dimension) + 2.0f);
        int top = (int) ((row *this.cell_dimension) + 2.0f);
        int right = (int) (((column+1) * this.cell_dimension) - 2.0f);
        int bottom =  (int) (((row+1) *this.cell_dimension) - 2.0f);

        // the display area.
        Rect areaRect = new Rect(left, top, right, bottom);
        RectF bounds = new RectF(areaRect);
        // measure text width
        bounds.right = paint.measureText(cellText, 0, cellText.length());
        // measure text height
        bounds.bottom = paint.descent() - paint.ascent();
        bounds.left += (areaRect.width() - bounds.right) / 2.0f;
        bounds.top += (areaRect.height() - bounds.bottom) / 2.0f;
        canvas.drawText(cellText, bounds.left, bounds.top - paint.ascent(), paint);
    }

    public void setFailed(boolean failed)
    {
        if(failed)
        {
            for(finishListners listener : finishs)
            {
                listener.onFinished(false);
            }
        }
        this.failed = failed;
    }
    public boolean ismarkingMode() {
        return markingMode;
    }
    public void setmarkingMode(boolean markingMode) {
        this.markingMode = markingMode;
    }
    public int getMarkedCount() {
        return markedCount;
    }
    public void setMarkedCount(int markedCount)
    {
        for(makingListners listener : this.markingChange)
        {
            listener.onMarkingChange(markedCount);
        }
        this.markedCount = markedCount;
    }
    public void addMarkingListners(makingListners listener)
    {
        this.markingChange.add(listener);
    }

    public interface makingListners
    {
        void onMarkingChange(int count);
    }
    public void addFinishs(finishListners listener)
    {
        this.finishs.add(listener);
    }

    public interface finishListners
    {
        void onFinished(boolean success);
    }


}
