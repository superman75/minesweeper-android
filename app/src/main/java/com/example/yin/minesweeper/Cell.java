package com.example.yin.minesweeper;

public class Cell {

    private boolean covered;
    private boolean marked;
    private int mineCount; // -1 for a mine


    public Cell(int mCount)
    {
        this.marked = false;
        this.covered = true;
        this.mineCount = mCount;
    }
    public boolean isCovered() {
        return covered;
    }

    public void setCovered(boolean covered) {
        this.covered = covered;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public int getMCount() {
        return mineCount;
    }
}
