package com.example.yin.minesweeper;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

	private long lastUpdate = 0;
	private float last_x;
	private float last_y;
	private float last_z;
	private int rows_columns = 10;
	private CustomView customView;
	private int mineCount = 20;
	private TextView mineCountView;
	private TextView markCountView;
	private Button statusButton;
	private Button resetButton;
	private static final int SHAKE_THRESHOLD = 1000;
	private Cell[][] cells;
	private boolean[][] isMine;

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */


	@Override
	protected void onCreate(Bundle savedInstanceState){

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initViews();
		reset();
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
				return super.onOptionsItemSelected(item);
	}


	@Override
	public void onSensorChanged(SensorEvent sensorEvent) {
		Sensor mySensor = sensorEvent.sensor;

		if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			float x = sensorEvent.values[0];
			float y = sensorEvent.values[1];
			float z = sensorEvent.values[2];
			long curTime = System.currentTimeMillis();

			if ((curTime - lastUpdate) > 100) {
				long diffTime = (curTime - lastUpdate);
				lastUpdate = curTime;

				float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

				if (speed > SHAKE_THRESHOLD) {
					reset();
				}

				last_x = x;
				last_y = y;
				last_z = z;

			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */

	@Override
	public void onStart() {
		super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.connect();
//		// ATTENTION: This was auto-generated to implement the App Indexing API.
//		// See https://g.co/AppIndexing/AndroidStudio for more information.
		AppIndex.AppIndexApi.start(client, getIndexApiAction0());
	}

	@Override
	public void onStop() {
		super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
		AppIndex.AppIndexApi.end(client, getIndexApiAction0());
//
//		// ATTENTION: This was auto-generated to implement the App Indexing API.
//		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.disconnect();
	}

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	public Action getIndexApiAction0() {
		Thing object = new Thing.Builder()
				.setName("Main Page") // TODO: Define a title for the content shown.
				// TODO: Make sure this auto-generated URL is correct.
				.setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
				.build();
		return new Action.Builder(Action.TYPE_VIEW)
				.setObject(object)
				.setActionStatus(Action.STATUS_TYPE_COMPLETED)
				.build();
	}


	private void initViews()
	{
		this.customView = (CustomView) findViewById(R.id.MineField);
		SensorManager senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		assert senSensorManager != null;
		Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
		statusButton = (Button) findViewById(R.id.statusButton);
		statusButton.setText(R.string.button_status1);
		resetButton = (Button) findViewById(R.id.resetButton);
		resetButton.setText(R.string.button_reset);
		mineCountView = (TextView) findViewById(R.id.MineCount);
		mineCountView.setText(String.valueOf(mineCount));
		markCountView = (TextView) findViewById(R.id.markedMineCount);
		markCountView.setText(String.valueOf(0));
		isMine = new boolean[rows_columns][rows_columns];
		cells = new Cell[rows_columns][rows_columns];
		this.customView.addMarkingListners(new CustomView.makingListners() {
			@Override
			public void onMarkingChange(int count) {
				showMarkingCount(count);
			}
		});

		this.customView.addFinishs(new CustomView.finishListners()
		{
			@Override
			public void onFinished(boolean success)
			{
				if(success)
				{
					createPlayerWonDialog();
				}
				if(!success)
				{
					createPlayerLostDialog();
				}
			}
		});

		statusButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				changeMode();
			}
		});

		resetButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				reset();
			}
		});

	}

	private void showMarkingCount(int count)
	{
		markCountView.setText(String.valueOf(count));
	}

	private void changeMode()
	{
		this.customView.setmarkingMode(!this.customView.ismarkingMode());
		if(!this.customView.ismarkingMode())
		{
			this.statusButton.setText(R.string.button_status1);
		}
		else
		{
			this.statusButton.setText(R.string.button_status2);
		}

	}

	private void reset()
	{
		this.initCells();
		this.customView.setFailed(false);
		this.customView.setmarkingMode(false);
		this.markCountView.setText("0");
		this.customView.setMarkedCount(0);
	}
	// init the two dimensional array of minesweepercells
	private void initCells()
	{
		this.cells = new Cell[rows_columns][rows_columns];
		int k = 0;
		for (int row = 0 ; row < rows_columns ; row++) for (int column = 0 ; column < rows_columns ; column++) isMine[row][column] = false;
		while(true)
		{
			int randR, randC;
			randR = (int) (Math.random() *rows_columns);
			randC = (int) (Math.random() *rows_columns);
			if(!isMine[randR][randC]){
				isMine[randR][randC] = true;
				k++;
			}
			if (k == mineCount) break;
		}
		// init the cells
		for(int i = 0; i < rows_columns; i++)
		{
			for(int j = 0; j < rows_columns; j++)
			{
				this.cells[i][j] = new Cell(getNumber(i,j));
			}
		}
		this.customView.setCell(this.cells);
		this.customView.invalidate();
	}
	// the mine count for a cell (measured by nearby cells)
	private int getNumber(int row, int column)
	{
		if(isMine[row][column])
		{
			return -1;
		}
		else
		{
			int mCount = 0;
			for(int i = -1; i < 2; i++)
			{
				for(int j = -1; j < 2; j++)
				{
					if(row+i < 0 || row+i >= rows_columns ||
							column+j < 0 || column+j >= rows_columns)
					{
						continue;
					}
					if(isMine[row+i][column+j])
					{
						mCount++;
					}
				}
			}
			return mCount;
		}
	}

	private void createPlayerWonDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("You Won").setTitle("Game Over");
		AlertDialog dialog = builder.create();
		dialog.show();
	}


	private void createPlayerLostDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("You Lost").setTitle("Game Over");
		AlertDialog dialog = builder.create();
		dialog.show();
	}

}
